﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Rigidbody _rigidbody;

    #region Unity

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Debug.DrawLine(transform.position, transform.position + _rigidbody.velocity, Color.red);

        if (_rigidbody.velocity != Vector3.zero)
            ScoreController.Instance.AddScore(1);

        Vector3 position = _rigidbody.position;
        position.z = 0;
        _rigidbody.position = position;
    }
    
    #endregion

    private void AddForce(Vector2 force)
    {
        _rigidbody.AddForce(force, ForceMode.Impulse);
    }

    public void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Wave"))
        {
            Wave collisionWave = collision.GetComponent<Wave>();

            Vector2 resul = this.transform.position - collisionWave.transform.position;
            Vector2 force = resul * collisionWave.Force;

            AddForce(force);
        }
    }
}