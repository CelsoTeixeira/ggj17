﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public enum Direction
{
    Left = 0,
    Right = 1,
    Up = 2, 
    Down = 3,
}

public static class SpawnOffscreen
{
    public static Vector3 GetRandomOffScreen()
    {
        Direction rng = (Direction)Random.Range(0, 4);

        Vector3 offScreen = GetScreenLimit(rng);

        switch (rng)
        {
            case Direction.Left:
                offScreen.x += Random.Range(10, 30);
                break;

            case Direction.Right:
                offScreen.x -= Random.Range(10, 30);
                break;

            case Direction.Up:
                offScreen.y += Random.Range(10, 30);
                break;

            case Direction.Down:
                offScreen.y -= Random.Range(10, 30);
                break;            
        }

        return offScreen;
    }

    public static Vector2 GetScreenLimit(Direction dir)
    {
        switch (dir)
        {
            case Direction.Left:
                return Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Random.Range(0, Screen.height)));
                
            case Direction.Right:
                return Camera.main.ScreenToWorldPoint(new Vector2(0, Random.Range(0, Screen.height)));

            case Direction.Up:
                return Camera.main.ScreenToWorldPoint(new Vector2(Random.Range(0, Screen.width), Screen.height));

            case Direction.Down:
                return Camera.main.ScreenToWorldPoint(new Vector2(Random.Range(0, Screen.width), 0));

            default:
                return Vector2.zero;               
        }
    }
}