﻿using UnityEngine;

public class WaveCreationIndicator : MonoBehaviour
{
    public int ScaleInitial;

    public int ScaleSize1;
    public int ScaleSize2;
    public int ScaleSize3;

    public Color ScaleColor1;
    public Color ScaleColor2;
    public Color ScaleColor3;

    private SpriteRenderer _renderer;

    private void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
        gameObject.SetActive(false);
    }


    public void StartIndicator(Vector3 posisiton)
    {
        transform.localScale = new Vector3(ScaleInitial, ScaleInitial, ScaleInitial);
        transform.localPosition = posisiton;
    }

    public void UpdateScale(float totalDuration, float currentDuration)
    {
        if (_renderer == null)
            _renderer = GetComponent<SpriteRenderer>();

        float step = totalDuration / 3;

        if (currentDuration <= step)
        {
            _renderer.color = ScaleColor1;
            transform.localScale = new Vector3(ScaleSize1, ScaleSize1, ScaleSize1);
        }
        else if (currentDuration <= step * 2)
        {
            _renderer.color = ScaleColor2;
            transform.localScale = new Vector3(ScaleSize2, ScaleSize2, ScaleSize2);
        }
        else if (currentDuration <= totalDuration)
        {
            _renderer.color = ScaleColor3;
            transform.localScale = new Vector3(ScaleSize3, ScaleSize3, ScaleSize3);
        }
    }


}