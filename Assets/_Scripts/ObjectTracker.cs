using System;
using System.Collections.Generic;
using Idleventures.Pooling;
using UnityEngine;

[Serializable]
public class ObjectTracker
{
    public PooledObject ObjectToTrack;

    public GameObject Player;

    public int InitialObjectsAmount = 7;
    public int MaxObjects = 14;
    
    public List<GameObject> Tracker = new List<GameObject>();

    public void Initialize()
    {
        //for (int i = 0; i < InitialObjectsAmount; i++)
        //{
        //    SpawnNewObject();
        //}
    }

    public void TrackerUpdate()
    {
        if (Tracker.Count >= MaxObjects)
            return;

        SpawnNewObject();
    }

    public void RemoveObject(GameObject toRemove)
    {
        if (Tracker.Contains(toRemove))
            Tracker.Remove(toRemove);
    }

    public void SpawnNewObject()
    {
        GameObject newSpawn = ObjectPoolController.Instance.GetObject(ObjectToTrack).gameObject;
        newSpawn.gameObject.SetActive(true);
        newSpawn.transform.position = SpawnOffscreen.GetRandomOffScreen();

        Enemy newEnemy = newSpawn.GetComponent<Enemy>();

        if (newEnemy != null)
        {
            newEnemy.OnEnemyDisable += RemoveObject;
        }

        Tracker.Add(newSpawn);
    }
}