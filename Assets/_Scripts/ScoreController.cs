﻿using DarkTonic.MasterAudio;
using UnityEngine;


public class ScoreController : MonoBehaviour
{
    #region Singleton

    private static ScoreController _instance;

    public static ScoreController Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<ScoreController>();
            return _instance;
        }
    }

    #endregion

    public int Score;

    public MusicController MusicController = new MusicController();

    public void Start()
    {
        MusicController.Initialize();
        
    }

    public void Update()
    {
        
    }

    public void AddScore(int toAdd)
    {
        Score += toAdd;
    }

    [ContextMenu("Change to second")]
    public void CrossFadeToSecond()
    {
        MusicController.ChangeToSecond();
    }

    [ContextMenu("Change to third")]
    public void CrossFadeToThird()
    {
        MusicController.ChangeToThird();
    }

    [ContextMenu("Change to fourth")]
    public void CrossFadeToFourth()
    {
        MusicController.ChangeToFourth();
    }
}