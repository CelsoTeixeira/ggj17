﻿using System;
using System.Diagnostics;
using DarkTonic.MasterAudio;
using UnityEngine;

/*  Para chamar um som é simplesmente fazer
        MasterAudio.PlaySound3DAtTransform()
        string = nome do som
        transform da posicao
        volume
 * 
 * 
 */

[Serializable]
public class MusicController
{
    public int ScoreToSecond = 100;
    public int ScoreToThird = 200;
    public int ScoreToFourth = 300;

    public float FadeDuration = 1f;

    public Transform Player;

    public void Initialize()
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform;

        MasterAudio.FadeBusToVolume("MainMusic2", 0, 0f);
        MasterAudio.FadeBusToVolume("MainMusic3", 0, 0f);
        MasterAudio.FadeBusToVolume("MainMusic4", 0, 0f);

        MasterAudio.PlaySound("Pirates Ahoy! - 1st drink", 1f);
        MasterAudio.PlaySound("Pirates Ahoy! - 2nd drink", 1f);
        MasterAudio.PlaySound("Pirates Ahoy! - 3rd drink", 1f);
        MasterAudio.PlaySound("Pirates Ahoy! - 4th drink", 1f);
    }

    public void ChangeToSecond()
    {
        MasterAudio.FadeBusToVolume("MainMusic", 0, FadeDuration);
        MasterAudio.FadeBusToVolume("MainMusic2", 1, FadeDuration);
    }

    public void ChangeToThird()
    {
        MasterAudio.FadeBusToVolume("MainMusic2", 0, FadeDuration);
        MasterAudio.FadeBusToVolume("MainMusic3", 1, FadeDuration);
    }

    public void ChangeToFourth()
    {
        MasterAudio.FadeBusToVolume("MainMusic3", 0, FadeDuration);
        MasterAudio.FadeBusToVolume("MainMusic4", 1, FadeDuration);
    }
}