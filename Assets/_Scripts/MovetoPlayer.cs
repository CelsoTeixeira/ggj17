﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovetoPlayer : MonoBehaviour
{

    public Transform _Player;
    public float speed;
    private Rigidbody _rigidbody;

    private void Awake()
    {
        if (_Player == null)
        {
            _Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        }
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = _Player.position - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.position = Vector3.MoveTowards(transform.position, _Player.transform.position, speed * Time.deltaTime);
    }

    private void AddForce(Vector2 force)
    {
        _rigidbody.AddForce(force, ForceMode.Impulse);
    }

    public void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Wave"))
        {
            UnityEngine.Debug.Log("Collision with wave.");

            Wave collisionWave = collision.GetComponent<Wave>();

            Vector2 resul = this.transform.position - collisionWave.transform.position;
            Vector2 force = resul * collisionWave.Force;

            AddForce(force);
        }
    }
}
