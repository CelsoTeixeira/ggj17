using UnityEngine;

namespace Idleventures.Pooling
{
    public class PooledObject : MonoBehaviour
    {
        public ObjectPool Pool { get; set; }

        public void ReturnToPool()
        {
            if (Pool != null)
                Pool.AddObjectToPool(this);
        }
    }
}