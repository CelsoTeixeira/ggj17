﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO(Celso): Need to add a method to not add overlapping enemies.

public class ObjectSpawn : MonoBehaviour
{
    public List<ObjectTracker> Trackers = new List<ObjectTracker>();

    private Coroutine _spawnUpdater;
    
    #region Unity

    private void Start()
    {
        InitialSetup();

        if (_spawnUpdater != null)
            StopCoroutine(_spawnUpdater);

        _spawnUpdater = StartCoroutine(SpawnUpdater());        
    }

    #endregion

    private void InitialSetup()
    {
        for (int i = 0; i < Trackers.Count; i++)
        {
            Trackers[i].Initialize();
        }
    }

    private IEnumerator SpawnUpdater()
    {
        while (true)
        {
            for (int i = 0; i < Trackers.Count; i++)
            {
                Trackers[i].TrackerUpdate();
            }

            yield return new WaitForEndOfFrame();
        }        
    }
}