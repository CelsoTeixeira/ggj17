﻿using System.Collections;
using System.Collections.Generic;
using Idleventures.Pooling;
using UnityEngine;

public class WaveCreation : MonoBehaviour
{
    #region Singleton

    private static WaveCreation _instance;

    public static WaveCreation Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<WaveCreation>();
            return _instance;
        }
    }

    #endregion

    public Wave WavePrefab;

    public int MaxPlayerWave = 1;

    private List<Wave> _playerWaves = new List<Wave>();

    public void RemoveWave(Wave toRemove)
    {
        if (_playerWaves.Contains(toRemove))
            _playerWaves.Remove(toRemove);
    }

    /// <summary>
    /// Create a wave but dont track it.
    /// User Input should call CreatePlayerWave!
    /// </summary>
    public void CreateWave(Vector2 initial, float duration)
    {
        Wave newWave = ObjectPoolController.Instance.GetObject(WavePrefab).GetComponent<Wave>();
        newWave.StartWave(initial, duration);              
    }

    /// <summary>
    /// Create a player wave and track it from start to end.
    /// </summary>
    public void CreatePlayerWave(Vector2 initial, float duration)
    {
        //If we're already at max waves created by the player, dont create a new one.
        if (_playerWaves.Count >= MaxPlayerWave)
            return;

        //Wave newWave = GameObject.Instantiate<Wave>(WavePrefab);
        Wave newWave = ObjectPoolController.Instance.GetObject(WavePrefab).GetComponent<Wave>();

        _playerWaves.Add(newWave);
        newWave.StartWave(initial, duration);
        newWave.OnWaveDead += RemoveWave;
    }
}
